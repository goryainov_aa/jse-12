package ru.goryainov.tm.entity;


import ru.goryainov.tm.enumerated.Role;
import ru.goryainov.tm.util.*;

public class User {

    /**
     * Идентификатор экземпляра класса с инициализацией случайным числом
     */
    private Long id = System.nanoTime();

    /**
     * Логин пользователя
     */
    private String login;

    /**
     * Хеш пароля пользователя
     */
    private String passwordHash;

    /**
     * Имя пользователя
     */
    private String firstName = "";

    /**
     * Фамилия пользователя
     */
    private String lastName = "";

    /**
     * Отчество пользователя
     */
    private String middleName = "";

    /**
     * Роль пользователя
     */
    private Role role;

    public User(String firstName, String lastName, String middleName, String login, String passwordHash, Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.role = role;
    }

    public User(String firstName, String lastName, String middleName, String login, String passwordHash) {
        this(firstName, lastName, middleName, login, passwordHash, Role.USER);
    }

    private User() {
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash =  HashMD5.md5(passwordHash);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
