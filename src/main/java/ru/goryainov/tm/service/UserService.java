package ru.goryainov.tm.service;

import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.enumerated.Role;
import ru.goryainov.tm.repository.UserRepository;
import ru.goryainov.tm.util.HashMD5;


import java.util.List;

public class UserService {

    private final UserRepository userRepository;


    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String firstName, final String lastName, final String middleName,
                       final String login, final String password, final String roleString) {
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        if (role == null) return null;
        return userRepository.create(firstName,lastName, middleName, login, password, role);
    }


    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.create(login, password);
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByIndex(final int index) {
        if (index < 0 || index > userRepository.size() - 1) return null;
        return userRepository.findByIndex(index);
    }

    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User findByLogin(final String login) {
        if (login == null) return null;
        return userRepository.findByLogin(login);
    }

    public User removeByIndex(final int index) {
        if (index < 0 || index > userRepository.size() - 1) return null;
        return userRepository.removeByIndex(index);
    }

    public User removeById(final Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByName(final String firstName, final String lastName, final String middleName) {
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        return userRepository.removeByName(firstName, lastName, middleName);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public User update(final Long id, final String firstName, final String lastName, final String middleName, final String login, final String roleName) {
        if (id == null) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (roleName == null || roleName.isEmpty()) return null;
        Role role = getRoleFromString(roleName);
        if (role == null) return null;
        return userRepository.updateById(id, firstName, lastName, middleName, login, role);
    }

    private Role getRoleFromString(final String roleString) {
            return Role.valueOf(roleString);
    }

    public User authentification(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.authentification(login, HashMD5.md5(password));
    }


}
