package ru.goryainov.tm.repository;


import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();
    private final UserRepository userRepository;

    public ProjectRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

   /* public Project create(final String name, final User user) {
        final Project project = new Project(name);
        project.setUserId(user.getId());
        projects.add(project);
        return project;
    }*/

    public Project create(final String name, final String description) {
        final Project project = new Project(name);
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description, final User user) {
        final Project project = new Project(name);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(user.getId());
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description, final Long userId) {
        final Project project = findById(id, userId);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    public void clear(final Long userId) {
        List<Project> userProjects = findByUserId(userId);
        for (Project project : userProjects) {
            projects.remove(project);
        }
    }

    /*public Project findByIndex(final int index) {
        if (index < 0 || index > projects.size() - 1) return null;
        return projects.get(index);
    }*/

    public Project findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty() || userId == null) return null;
        for (final Project project : projects) {
            if (project.getUserId() == null || !project.getUserId().equals(userId)) continue;
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public Project findById(final Long id, final Long userId) {
        if (id == null || userId == null) return null;
        for (final Project project : projects) {
            if (project.getUserId() == null || !project.getUserId().equals(userId)) continue;
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public List<Project> findByUserId(final Long userId) {
        List<Project> userProjects = new ArrayList<>();
        for (Project project : projects) {
            if (project.getUserId() == null || !project.getUserId().equals(userId)) continue;
            userProjects.add(project);
        }
        return userProjects;
    }


    public Project removeById(final Long id, final Long userId) {
        final Project project = findById(id, userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name, final Long userId) {
        final Project project = findByName(name, userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /*public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }*/

    public List<Project> findAll(final Long userId) {
        List<Project> userProjects = findByUserId(userId);
        return userProjects;
    }

    public List<Project> findAll() {
        return projects;
    }

}
